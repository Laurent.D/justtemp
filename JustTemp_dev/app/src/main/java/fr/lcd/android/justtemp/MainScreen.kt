package fr.lcd.android.justtemp

import android.content.Intent

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun MainScreen(mainViewModel : MainViewModel, getTempFunc : () -> Int)
{
    Box(
        contentAlignment = Alignment.TopEnd
    ) {
        SettingsButton()
    }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Row(horizontalArrangement = Arrangement.SpaceAround) {
            Temperature(mainViewModel)
        }

        Spacer(modifier = Modifier.height(20.dp))

        Row(horizontalArrangement = Arrangement.SpaceAround) {
            RefreshButton(getTempFunc, mainViewModel)
        }
    }
}

@Composable
fun Temperature(mainViewModel: MainViewModel, modifier: Modifier = Modifier)
{
    val tempString by mainViewModel.temperatureValueFlow.collectAsState()
    Text(
        text = tempString + stringResource(R.string.celsius),
        fontSize = 80.sp,
        modifier = modifier
    )
}

@Composable
fun RefreshButton(getTempFunc : () -> Int, mainViewModel: MainViewModel)
{
    Button(onClick = {
        mainViewModel.setTemperature(getTempFunc())
    }) {
        Text(text = stringResource(R.string.refresh), fontSize = 30.sp)
    }
}

@Composable
fun SettingsButton() {
    val context = LocalContext.current

    Icon(
        imageVector = Icons.Default.Settings,
        contentDescription = stringResource(R.string.settings_icon_desc),
        modifier = Modifier
            .size(60.dp)
            .padding(horizontal = 16.dp, vertical = 16.dp)
            .clickable {
                val intent = Intent(context, SettingsActivity::class.java)
                context.startActivity(intent)
            }
    )
}