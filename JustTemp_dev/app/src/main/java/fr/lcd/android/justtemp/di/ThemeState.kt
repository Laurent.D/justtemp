package fr.lcd.android.justtemp.di

data class ThemeState(val isDarkMode: Boolean)