package fr.lcd.android.justtemp

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner

class MainViewModel(private val savedStateHandle : SavedStateHandle) : ViewModel()
{
    companion object
    {
        private const val TEMP_KEY        : String = "temperature"
        private const val TEMP_INIT_VALUE : String = "--.-"

        fun provideFactory(
            owner: SavedStateRegistryOwner,
            defaultArgs: Bundle? = null,
        ): AbstractSavedStateViewModelFactory =
            object : AbstractSavedStateViewModelFactory(owner, defaultArgs)
            {
                override fun <T : ViewModel> create(
                    key: String,
                    modelClass: Class<T>,
                    handle: SavedStateHandle
                ): T {
                    if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
                        @Suppress("UNCHECKED_CAST") //NON-NLS
                        return MainViewModel(handle) as T
                    }
                    throw IllegalArgumentException("Invalid passed viewModel")
                }
            }
    }

    val temperatureValueFlow = savedStateHandle.getStateFlow(TEMP_KEY, TEMP_INIT_VALUE)

    /**************************************************************************/
    fun restoreTemperatureString(initialTempString: String)
    {
        savedStateHandle[TEMP_KEY] = initialTempString
    }

    /**************************************************************************/
    fun getTemperatureString() : String
    {
        return temperatureValueFlow.value
    }

    /**************************************************************************/
    fun setTemperature(temperature: Int)
    {
        savedStateHandle[TEMP_KEY] = (temperature / 10.0).toString()
    }
}


