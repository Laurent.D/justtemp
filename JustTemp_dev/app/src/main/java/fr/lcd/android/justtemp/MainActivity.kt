package fr.lcd.android.justtemp

import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import dagger.hilt.android.AndroidEntryPoint
import fr.lcd.android.justtemp.ui.theme.JustTempTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity()
{
    companion object {
        private const val SAVED_TEMPERATURE = "SAVED_TEMPERATURE"
    }

    private val _viewModel: MainViewModel by viewModels {
        MainViewModel.provideFactory( this)
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null)
        {
            _viewModel.restoreTemperatureString(savedInstanceState.getString(SAVED_TEMPERATURE, ""))
        }
        else
        {
            _viewModel.setTemperature(getTemperature())
        }
        setContent {
            JustTempTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainScreen(mainViewModel = _viewModel, getTempFunc = ::getTemperature)
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle)
    {
        super.onSaveInstanceState(outState)
        outState.putString(SAVED_TEMPERATURE, _viewModel.getTemperatureString())
    }

    private fun getTemperature() : Int
    {
        val intentFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        val batteryStatus = applicationContext.registerReceiver(null, intentFilter)
        return batteryStatus!!.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1)
    }
}





