package fr.lcd.android.justtemp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class JustTempApp: Application()