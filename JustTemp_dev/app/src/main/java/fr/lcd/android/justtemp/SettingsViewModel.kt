package fr.lcd.android.justtemp

import androidx.datastore.preferences.core.edit
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

import fr.lcd.android.justtemp.di.DataStoreUtil
import fr.lcd.android.justtemp.di.DataStoreUtil.Companion.IS_DARK_MODE_KEY
import fr.lcd.android.justtemp.di.ThemeState

@HiltViewModel
class SettingsViewModel @Inject constructor(dataStoreUtil: DataStoreUtil) : ViewModel()
{
    private var _darkThemeState = MutableStateFlow(ThemeState(false))
    val         darkThemeState: StateFlow<ThemeState> = _darkThemeState.asStateFlow()

    private val dataStore = dataStoreUtil.store

    init {
        viewModelScope.launch(Dispatchers.IO) {
            dataStore.data.map { preferences ->
                ThemeState(preferences[IS_DARK_MODE_KEY] ?: false)
            }.collect {
                _darkThemeState.value = it
            }
        }
    }

    fun toggleThemeSwitch()
    {
        viewModelScope.launch(Dispatchers.IO) {
            dataStore.edit { preferences ->
                preferences[IS_DARK_MODE_KEY] = !(preferences[IS_DARK_MODE_KEY] ?: false)
            }
        }
    }
}