package fr.lcd.android.justtemp.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import javax.inject.Inject

class DataStoreUtil @Inject constructor(context: Context)
{
    val store = context.dataStore

    companion object {
        private const val STORE_NAME = "settings"
        private const val DARK_MODE_KEY = "dark_mode"
        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = STORE_NAME)
        val IS_DARK_MODE_KEY = booleanPreferencesKey(DARK_MODE_KEY)
    }
}